package com.example.minhha_25._bai81;

import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("bai8.1")
public class Controller {
    @GetMapping("/nam-nhuan")
    public String kiemTraNamNhuan(@RequestParam int nam) {
        if ((nam % 4 == 0 && nam % 100 != 0) || (nam % 400 == 0)) {
            return nam + " la nam nhuan";
        } else {
            return nam + " khong phai nam nhuan";
        }
    }
}
